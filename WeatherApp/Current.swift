//
//  Current.swift
//  WeatherApp
//
//  Created by Chinedu Amadi-Ndukwe on 19/11/2014.
//  Copyright (c) 2014 Chinedu Amadi-Ndukwe. All rights reserved.
//

import Foundation
import UIKit

struct Current {
    
    var currentTime: String?
    var temperature: Int
    var humidity: Double
    var precipProbability: Double         //Chance of rain
    var summary: String
    var icon: UIImage?

    
    init(weatherDictionary:NSDictionary) {
        let currentWeather = weatherDictionary["currently"] as NSDictionary
        
        temperature = currentWeather["temperature"] as Int
        humidity = currentWeather["humidity"] as Double
        precipProbability = currentWeather["precipProbability"] as Double
        summary = currentWeather["summary"] as String
        
        let currentTimeIntValue = currentWeather["time"] as Int
        currentTime = dateStringFromUnixTime(currentTimeIntValue)
        
        let iconString = currentWeather["icon"] as String
        //icon = weatherIconString(iconString)
        
        let fahrenheit = currentWeather["temperature"] as Int
        temperature = convertToCelsius(fahrenheit)
        
    }

    
    func dateStringFromUnixTime(unixTime: Int) -> String {
        let timeInSeconds = NSTimeInterval(unixTime)
        let weatherDate = NSDate(timeIntervalSince1970: timeInSeconds)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .ShortStyle
        
        return dateFormatter.stringFromDate(weatherDate)
    }
    
    func convertToCelsius(fahrenheit: Int) -> Int {
        return Int(5.0 / 9.0 * (Double(fahrenheit) - 32.0))
    }
    
    
    
//    func weatherIconFromString(stringIcon: String) -> UIImage {
//        var imageName:String
//        
//        switch stringIcon {
//                case "clear-day":
 //                   imageName = "clear-day"
    //            case "clear-night":
    //                   imageName = "clear-night"

    //            case "rain":
    //                   imageName = "rain"

    //            case "snow":
    //                   imageName = "snow"

    //            case "sleet":
    //                   imageName = "sleet"

    //            case "wind":
    //                   imageName = "wind"
    //            case "fog":
    //                   imageName = "fog"
    //            case "cloudy":
    //                   imageName = "cloudy"
    //            case "partly-cloudy-day":
    //                   imageName = "partly-cloudy-day"
    //            case "partly-cloudy-night":
    //                   imageName = "partly-cloudy-night"
    //            default:
    //                   imageName = "default"

//        }
                // var iconImage = UIImage(named: imageName)
                // return iconImage
//    }
}