//
//  ViewController.swift
//  WeatherApp
//
//  Created by Chinedu Amadi-Ndukwe on 19/11/2014.
//  Copyright (c) 2014 Chinedu Amadi-Ndukwe. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var currentTimeLabel: UILabel!
    
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var humidityLabel: UILabel!
    
    @IBOutlet weak var precipitationLabel: UILabel!
    
    @IBOutlet weak var degreeSign: UIImageView!
    
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var summaryLabel: UILabel!
    
  
    @IBOutlet weak var refreshActivityIndicator: UIActivityIndicatorView!
   
    private let apiKey = "5829879efdfb14b9b0281391412ac7d7"
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        degreeSign.image = UIImage(named: "degree.png")
        
        refreshActivityIndicator.hidden = true
        getCurrentWeatherData() 
        }

    func getCurrentWeatherData() -> Void {
        let baseURL = NSURL(string: "https://api.forecast.io/forecast/\(apiKey)/")
        
        let forecastURL = NSURL(string: "52.405302,-1.499861", relativeToURL: baseURL)
        
        let sharedSession = NSURLSession.sharedSession()
        let downTask: NSURLSessionDownloadTask = sharedSession.downloadTaskWithURL(forecastURL!, completionHandler: { (location:NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
            //println(response)
            
            //            var urlContents = NSString(contentsOfURL: location, encoding: NSUTF8StringEncoding, error: nil)
            //            println(urlContents)
            
            let dataObject = NSData(contentsOfURL: location)
            let weatherDictionary:NSDictionary = NSJSONSerialization.JSONObjectWithData(dataObject!, options: nil, error: nil) as NSDictionary
            //println(weatherDictionary)
            //
            //            let currentWeatherDictionary: NSDictionary = weatherDictionary["currently"] as NSDictionary
            //
            //            println(currentWeatherDictionary)
            
            let currentWeather = Current(weatherDictionary: weatherDictionary)
            //println(currentWeather.currentTime!)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                self.temperatureLabel.text = "\(currentWeather.temperature)"
                
                self.currentTimeLabel.text = "At \(currentWeather.currentTime!) It is"
                
                self.humidityLabel.text = "\(currentWeather.humidity)"
                
                self.precipitationLabel.text = "\(currentWeather.precipProbability)"
                
                self.summaryLabel.text = "\(currentWeather.summary)"
                
                self.temperatureLabel.text = "\(currentWeather.temperature)"
                //Stop refresh animation
                println("hello")
                self.refreshActivityIndicator.stopAnimating()
                self.refreshActivityIndicator.hidden = true
                self.refreshButton.hidden = false
            })
        })
        
        downTask.resume()

    }
    
    
    
    @IBAction func refresh() {
        
        getCurrentWeatherData()
        
        refreshButton.hidden = true
        refreshActivityIndicator.hidden = false
        
        refreshActivityIndicator.startAnimating()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

